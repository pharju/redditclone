package utils;

import java.sql.Timestamp;

public class LowLevelLogger implements I_LogStrategy {

	@Override
	public void log(String pLogMessage, LogFilterDepth pFilterDepth) {

		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime()) + ": " + pLogMessage);

	}

}
