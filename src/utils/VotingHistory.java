package utils;

/* Had to implement this as a class since Java doesn't support structs... */
public class VotingHistory {
	private int id = 0;
	private VoteType voteType;
	
	public VotingHistory(int id, VoteType voteType)
	{
		this.id = id;
		this.voteType = voteType;
	}
	public int getId()
	{
		return this.id;
	}
	public VoteType getVoteType()
	{
		return this.voteType;
	}
	public void setVoteType(VoteType voteType)
	{
		this.voteType = voteType;
	}
	public void negateVote()
	{
		if(this.voteType == VoteType.DOWNVOTED)
		{
			this.voteType = VoteType.UPVOTED;
		}
		else if(this.voteType == VoteType.UPVOTED)
		{
			this.voteType = VoteType.DOWNVOTED;
		}
	}
}
