package utils;

import java.util.Date;

import javax.faces.context.FacesContext;

public class TimeStampCalculation {
	private Date actualTimeStamp = null;
	private Date theTimeStamp = null;

	public TimeStampCalculation (Date timeStamp) {
		this.actualTimeStamp = new Date();
		this.theTimeStamp = timeStamp;
	}

	@SuppressWarnings("deprecation")
	public String CalculateDifferenceToNow () {
		FacesContext context = FacesContext.getCurrentInstance();
		String strTimeInfo = "";
		int iValue = 0;

		actualTimeStamp = new Date();
		
		if (this.theTimeStamp.compareTo(this.actualTimeStamp) == -1) {
			iValue = this.actualTimeStamp.getYear() - this.theTimeStamp.getYear();
			
			if (iValue > 0) {
				if (iValue == 1) {
					strTimeInfo = Utils.getResourceText(context, "msgs", "yearAgo", iValue);
				}
				else {
					strTimeInfo = Utils.getResourceText(context, "msgs", "yearsAgo", iValue);
				}
			}
			else {
				iValue = this.actualTimeStamp.getMonth() - this.theTimeStamp.getMonth();
				
				if (iValue > 0) {
					if (iValue == 1) {
						strTimeInfo = Utils.getResourceText(context, "msgs", "monthAgo", iValue);
					}
					else {
						strTimeInfo = Utils.getResourceText(context, "msgs", "monthsAgo", iValue);
					}
				}
				else {
					iValue = this.actualTimeStamp.getDay() - this.theTimeStamp.getDay();
					
					if (iValue > 0) {
						if (iValue == 1) {
							strTimeInfo = Utils.getResourceText(context, "msgs", "dayAgo", iValue);
						}
						else {
							strTimeInfo = Utils.getResourceText(context, "msgs", "daysAgo", iValue);
						}
					}
					else {
						iValue = this.actualTimeStamp.getHours() - this.theTimeStamp.getHours();
						
						if (iValue > 0) {
							if (iValue == 1) {
								strTimeInfo = Utils.getResourceText(context, "msgs", "hourAgo", iValue);
							}
							else {
								strTimeInfo = Utils.getResourceText(context, "msgs", "hoursAgo", iValue);
							}
						}
						else {
							iValue = this.actualTimeStamp.getMinutes() - this.theTimeStamp.getMinutes();

							if (iValue > 0) {
								if (iValue == 1) {
									strTimeInfo = Utils.getResourceText(context, "msgs", "minuteAgo", iValue);
								}
								else {
									strTimeInfo = Utils.getResourceText(context, "msgs", "minutesAgo", iValue);
								}
							}
							else {
								iValue = this.actualTimeStamp.getSeconds() - this.theTimeStamp.getSeconds();

								if (iValue > 0) {
									if (iValue == 1) {
										strTimeInfo = Utils.getResourceText(context, "msgs", "secondAgo", iValue);
									}
									else {
										strTimeInfo = Utils.getResourceText(context, "msgs", "secondsAgo", iValue);
									}
								}							
							}
						}
					}
				}
			}
		}
		return strTimeInfo;
	}
}