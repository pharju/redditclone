package utils;

import java.sql.Timestamp;

public class MediumLevelLogger implements I_LogStrategy{

	@Override
	public void log(String pLogMessage, LogFilterDepth pFilterDepth) {

		if (pFilterDepth == LogFilterDepth.MEDIUM){
			java.util.Date date= new java.util.Date();
			System.out.println(new Timestamp(date.getTime()) + ": " + pLogMessage);
		}
	}
}
