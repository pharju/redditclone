package utils;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;


public class Utils {
	
	private static I_LogStrategy logger = new MediumLevelLogger();

	public static String getResourceText(FacesContext ctx, String bundleName, String key, Object... args){
		
		String text;
		try {
			Application app = ctx.getApplication();
			ResourceBundle bundle = app.getResourceBundle(ctx, bundleName);
			text = bundle.getString(key);
		} catch (MissingResourceException e){
			return "???_" + key + "_???";
		}
		
		if (args != null) text = MessageFormat.format(text, args);
		return text;
	}
	
	public static boolean isMac() {
		 
		String os = System.getProperty("os.name").toLowerCase();
		// Mac
		return (os.indexOf("mac") >= 0);
 
	}
	
	public static void log (String pLogMessage){
		log(pLogMessage, LogFilterDepth.MEDIUM);
	}

	public static void log (String pLogMessage, LogFilterDepth pLogFilterDepth){
		logger.log(pLogMessage, pLogFilterDepth);
	}
}
