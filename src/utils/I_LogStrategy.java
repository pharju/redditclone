package utils;

public interface I_LogStrategy {

	public void log(String pLogMessage, LogFilterDepth pFilterDepth);

}
