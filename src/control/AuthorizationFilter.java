package control;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.Utils;

public class AuthorizationFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, 
			FilterChain chain) throws ServletException, IOException {    
	      	HttpServletRequest req = (HttpServletRequest) request;
	      	LoginBean login = (LoginBean) req.getSession().getAttribute("loginBean");

	       if (   (login != null && login.getUser() != null)
	    	   || req.getRequestURI().equals("/RedditClone/")
	    	   || req.getRequestURI().contains("login.xhtml") 
	    	   || req.getRequestURI().contains("LoggedOut.xhtml")
	    	   || req.getRequestURI().contains("register.xhtml")) {
	           // User is logged in, so just continue request.
	           chain.doFilter(request, response);
	       } else {
	           // User is not logged in, so redirect to index.
	    	   Utils.log("No user logged in => " 
	    			   		+ req.getRequestURI() 
	    			   		+ " redirected to login.xhtml, " );
	           HttpServletResponse res = (HttpServletResponse) response;
	           //res.sendRedirect(req.getContextPath() + "/login.xhtml");
	           res.sendRedirect(req.getContextPath());
	       }
	   }

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
}
