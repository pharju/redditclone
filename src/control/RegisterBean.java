package control;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

import model.User;
import model.UserException;
import model.UserManager;
import utils.Utils;

@ManagedBean(name="registerBean")
@RequestScoped
public class RegisterBean {

	protected String login;
	protected String password;
	protected String password2;
	protected String name;
	protected String mail;
	
	@ManagedProperty(value="#{userManager}")
	private UserManager myUserManager;
	@ManagedProperty(value="#{loginBean}")
	private LoginBean myLogin;
	
	public String register (){
		
		User user = new User();
		user.setLogin(login);
		user.setPassword(password);
		try {
			myUserManager.register(user);
		} catch(UserException e) {
			return "register.xhtml";
		}

		myLogin.setUser(user);

		return myLogin.navigateToApproppriatePage();
	}
	
	
	public void validateNickName (FacesContext context, UIComponent component, Object value) {
		
        String nickName = (String)value;
        
        Utils.log("Validate nick name " + nickName);

        if (! myUserManager.checkIfFree(nickName)){
            ((UIInput)component).setValid(false);
			String errorMsg = Utils.getResourceText(context, "msgs", "userAlreadyExists");
            FacesMessage msg = new FacesMessage(errorMsg);
            context.addMessage(component.getClientId(context), msg);
       	
        }
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword2() {
		return password2;
	}
	public void setPassword2(String password2) {
		this.password2 = password2;
	}
	public void setMyUserManager(UserManager myUserManager) {
		this.myUserManager = myUserManager;
	}
	public void setMyLogin(LoginBean myLogin) {
		this.myLogin = myLogin;
	}
}
