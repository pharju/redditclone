package control;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utils.Utils;

public class SessionTimeoutFilter implements Filter{

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
	    HttpServletRequest req = (HttpServletRequest) request;
	    HttpServletResponse res = (HttpServletResponse) response;
	    String requestPath = req.getRequestURI();
	    if (   req.getRequestedSessionId() != null
	    	&& req.getRequestURI().contains(".xhtml")
	    	&& ! req.getRequestURI().contains("login.xhtml")
	    	&& !req.isRequestedSessionIdValid()) {
	    	Utils.log("SessionTimeoutFilter, redirect to " + req.getContextPath());
	    	res.sendRedirect(req.getContextPath());
	    } else {
	    	if (req.getRequestURI().contains("login.xhtml")){
	    		Utils.log("login.xhtml => kein Filter");
	    	}
	        chain.doFilter(request, response);
	    }
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
	
}
