package control;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

import model.User;
import model.UserException;
import model.UserManager;
import utils.Utils;
 
@ManagedBean(name="loginBean")
@SessionScoped
public class LoginBean implements Serializable {
	
	private String name;
	private String password;
	private String welcome = null;
	private boolean submissionSelected = false;
	
	private User user;
	
	@ManagedProperty(value="#{userManager}")
	private UserManager myUserManager;

	public String login (){
		
		try {
			user = myUserManager.login(name, password);
	
			if(user != null) 
			{
				return navigateToApproppriatePage();
			}
		}
		catch(UserException e) {
			FacesContext context = FacesContext.getCurrentInstance();
						
            String errorMsg = Utils.getResourceText(context, "msgs", "invalidUserAndOrPassword");
            FacesMessage msg = new FacesMessage(errorMsg);
            context.addMessage("formLogin:buttonLogin", msg);
			return "login.xhtml";
		}
		return "login.xhtml";
	}


	public String navigateToApproppriatePage() 
	{
		if(this.submissionSelected == false)
		{
			return "frontpageLoggedIn.xhtml";
		}
		else
		{
			return "detailViewLoggedIn.xhtml";
		}
	}
	

	public void checkLogin (FacesContext context, UIComponent component, Object value) {
		
        String nickName = (String)value;

        if (myUserManager.checkIfFree(nickName)){
            ((UIInput)component).setValid(false);
            String errorMsg = Utils.getResourceText(context, "msgs", "userDoesNotExist");
            FacesMessage msg = new FacesMessage(errorMsg);
            context.addMessage(component.getClientId(context), msg);
       	
        }
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserManager getMyUserManager() {
		return myUserManager;
	}

	public void setMyUserManager(UserManager myUserManager) {
		this.myUserManager = myUserManager;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		if (user != null){
			Utils.log("setUser: user -> " + user.getLogin() );
		}
		this.user = user;
	}

	public String getLoginName() {
		if (user != null) {
			Utils.log("getLoginName: user = " + user.getLogin() );
			return user.getLogin();
		}
		else{
			return null;
		}
	}
	public void submissionIsSelected (boolean submissionSelected)
	{
		this.submissionSelected = submissionSelected;
	}
}
