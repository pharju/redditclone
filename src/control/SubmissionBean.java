package control;

import java.io.Serializable;
import java.util.Map;


import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import model.Submission;
import model.Comment;
import model.SubmissionManager;
import model.User;

import org.primefaces.context.RequestContext;

import utils.Utils;
import utils.VoteType;

@ManagedBean(name="submissionBean")
@RequestScoped
public class SubmissionBean implements Serializable {
	
	private String message;
	private Submission selectedSubmission;
	
	@ManagedProperty(value="#{loginBean}")
	private LoginBean myLogin;

	@ManagedProperty(value="#{frontpageBean}")
	private FrontpageBean myFrontpage;
	
	public String addCommentEntry(){
		
		selectedSubmission = myFrontpage.getSelectedSubmission();
		Comment newComment = new Comment(message, myLogin.getLoginName(), selectedSubmission);
		selectedSubmission.addComment(newComment);
		return "detailViewLoggedIn.xhtml";
	}
	
	public int getSubmissionVoteCount()
	{
		selectedSubmission = myFrontpage.getSelectedSubmission();
		return selectedSubmission.getSubmissionVoteCount();
	}
	public String getSubmissionTimestamp()
	{
		selectedSubmission = myFrontpage.getSelectedSubmission();
		return selectedSubmission.getSubmissionTimestamp();
	}
	public String getSubmissionAuthor()
	{
		selectedSubmission = myFrontpage.getSelectedSubmission();
		return selectedSubmission.getSubmissionAuthor();
	}
	public String getSubmissionTitle()
	{
		selectedSubmission = myFrontpage.getSelectedSubmission();
		return selectedSubmission.getSubmissionTitle();
	}
	public String getSubmissionLinkString()
	{
		selectedSubmission = myFrontpage.getSelectedSubmission();
		return selectedSubmission.getSubmissionLinkString();
	}
	public void upVoteSubmission()
	{
		myFrontpage.upVoteSubmission();
	}
	public void downVoteSubmission()
	{
		myFrontpage.downVoteSubmission();
	}
	public void findCommentAndUpvote()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		Map map = context.getExternalContext().getRequestParameterMap();
		String selectedCommentIdString = (String) map.get("commentId");
		int selectedCommentId = Integer.parseInt(selectedCommentIdString);
		myFrontpage.upVoteComment(selectedCommentId);
	}
	public void findCommentAndDownvote()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		Map map = context.getExternalContext().getRequestParameterMap();
		String selectedCommentIdString = (String) map.get("commentId");
		int selectedCommentId = Integer.parseInt(selectedCommentIdString);
		myFrontpage.downVoteComment(selectedCommentId);	}
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMyFrontpage(FrontpageBean myFrontpage) {
		this.myFrontpage = myFrontpage;
	}

	public Submission getSelectedSubmission() {
		return  this.myFrontpage.getSelectedSubmission();
	}

	public void setMyLogin(LoginBean myLogin) {
		this.myLogin = myLogin;
	}	
	public String leaveSubmission()
	{
		myFrontpage.resetSelectedSubmission();		
		if(myLogin.getLoginName() == null)
		{
			return "frontpageLoggedOut.xhtml";
		}
		else
		{
			return "frontpageLoggedIn.xhtml";
		}
	}
}
