package control;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import model.Comment;
import model.Submission;
import model.SubmissionManager;
import model.User;

import utils.Utils;
import utils.VoteType;

@ManagedBean(name="frontpageBean")
@SessionScoped
public class FrontpageBean implements Serializable {

	private Submission selectedSubmission = null;
	private Comment selectedComment = null;
	
	private String selectedSubmissionTitle;
	private String newSubmissionTitle;
	private String newSubmissionUrl;
	

	private Hashtable<String,Object> submissionList; // for Selection List

	@ManagedProperty(value="#{submissionManager}")
	private SubmissionManager mySubmissionManager;
	
	@ManagedProperty(value="#{loginBean}")
	private LoginBean myLogin;
	
	
	public String addSubmission(){
		// synchronized, because validation and entry are not atomic
		synchronized (mySubmissionManager.getSubmissions()){
			selectedSubmission = mySubmissionManager.getSubmissions().get(newSubmissionTitle);
			if (selectedSubmission == null){
				Submission newSubmission = new Submission(newSubmissionUrl, myLogin.getLoginName(), newSubmissionTitle);
				selectedSubmission = newSubmission;
				selectedSubmissionTitle = newSubmission.getSubmissionTitle();
				mySubmissionManager.addSubmission(newSubmission);
			}
			else{
				// TODO: Handle the case where the submission with the give title already exists
			}
		}
		return "detailViewLoggedIn.xhtml";	
	}
	public void setNewSubmissionTitle(String newSubmissionTitle)
	{
		this.newSubmissionTitle = newSubmissionTitle;
	}
	public String getNewSubmissionTitle() {
		return newSubmissionTitle;
	}
	public String openDetailViewPage()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		Map map = context.getExternalContext().getRequestParameterMap();
		String selectedSubmissionTitle= (String) map.get("title"); 
		selectedSubmission = mySubmissionManager.getSubmissions().get(selectedSubmissionTitle);
		myLogin.submissionIsSelected(true);
		if(myLogin.getLoginName() != null)
		{
			return "detailViewLoggedIn.xhtml";
		}
		else
		{
			return "detailViewLoggedOut.xhtml";
		}
	}

	public void validateSubmissionTitle (FacesContext context, UIComponent component, Object value) {
		
        String submissionTitle = (String)value;
        
//        Utils.log("Validate nick name " + nickName);

        if (! mySubmissionManager.checkIfFree(submissionTitle)){
            ((UIInput)component).setValid(false);
			String errorMsg = Utils.getResourceText(context, "msgs", "titleAlreadyExists");
            FacesMessage msg = new FacesMessage(errorMsg);
            context.addMessage(component.getClientId(context), msg);
       	
        }
	}
	public void findSubmissionAndUpvote()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		Map map = context.getExternalContext().getRequestParameterMap();
		String selectedSubmissionTitle= (String) map.get("title"); 
		selectedSubmission = mySubmissionManager.getSubmissions().get(selectedSubmissionTitle);
		upVoteSubmission();
	}

	public void findSubmissionAndDownvote()
	{
		FacesContext context = FacesContext.getCurrentInstance();
		Map map = context.getExternalContext().getRequestParameterMap();
		String selectedSubmissionTitle= (String) map.get("title"); 
		selectedSubmission = mySubmissionManager.getSubmissions().get(selectedSubmissionTitle);
		downVoteSubmission();
	}

	public void upVoteSubmission()
	{
		User myUser;
		try
		{
			myUser = myLogin.getUser();	
			if(myUser.checkVoteValidityAndRegisterSubmissionVote(selectedSubmission.getSubmissionId(), VoteType.UPVOTED))
			{
				selectedSubmission.upVoteSubmission();
			}
		}catch (Exception e)
		{
			System.out.println(e);
		}
	}
	public void downVoteSubmission()
	{
		User myUser;
		try
		{
			myUser = myLogin.getUser();	
			if(myUser.checkVoteValidityAndRegisterSubmissionVote(selectedSubmission.getSubmissionId(), VoteType.DOWNVOTED))
			{
				selectedSubmission.downVoteSubmission();
			}
		}catch (Exception e)
		{
			System.out.println(e);
		}
	}
	public void upVoteComment(int selectedCommentId)
	{
		User myUser;
		try
		{
			myUser = myLogin.getUser();	
			if(myUser.checkVoteValidityAndRegisterCommentVote(selectedCommentId, VoteType.UPVOTED))
			{
				selectedComment = selectedSubmission.getCommentWithId(selectedCommentId);
				if(selectedComment != null)
				{
					selectedComment.upVote();
				}
			}
		}catch (Exception e)
		{
			System.out.println(e);
		}
	}
	public void downVoteComment(int selectedCommentId)
	{
		User myUser;
		try
		{
			myUser = myLogin.getUser();	
			if(myUser.checkVoteValidityAndRegisterCommentVote(selectedCommentId, VoteType.DOWNVOTED))
			{
				selectedComment = selectedSubmission.getCommentWithId(selectedCommentId);
				if(selectedComment != null)
				{
					selectedComment.downVote();
				}
			}
		}catch (Exception e)
		{
			System.out.println(e);
		}
	}

	public void setNewSubmissionUrl(String newSubmissionUrl)
	{
		this.newSubmissionUrl = newSubmissionUrl;
	}
	public String getnewSubmissionUrl()
	{
		return this.newSubmissionUrl;
	}
	public Submission getSelectedSubmission() {
		return selectedSubmission;
	}
	public void resetSelectedSubmission() {
		this.selectedSubmission = null;
		myLogin.submissionIsSelected(false);
	}
	public void setSelectedSubmission(Submission selectedSubmission) {
		selectedSubmissionTitle = selectedSubmission.getSubmissionTitle();
//		Utils.log("selectedChatName -> " + selectedChatName);
		this.selectedSubmission = selectedSubmission;
	}
	public String getSelectedSubmissionUrl() {
		return selectedSubmission.getSubmissionLinkString();
	}

	public void setMySubmissionManager(SubmissionManager mySubmissionManager) {
		this.mySubmissionManager = mySubmissionManager;
	}

	public SubmissionManager getSubmissionManager(){
		return mySubmissionManager;
	}
	public void setMyLogin(LoginBean myLogin) {
		this.myLogin = myLogin;
	}
	public String getSelectedSubmissionTitle() {
		return selectedSubmissionTitle;
	}
	public String logout(){
		selectedSubmission = null;
		myLogin.setName(null);
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "login.xhtml";
	}


}
