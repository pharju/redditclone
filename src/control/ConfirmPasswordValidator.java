package control;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import utils.Utils;

public class ConfirmPasswordValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String password  = (String) value;
        String password2 = (String) component.getAttributes().get("password2");

        Utils.log("password validation " + password + " " + password2);
        
        if (password == null || password2 == null || password2.equals("")) {
            return; // let required="true" do its job.
        }

        if (!password.equals(password2)) {

        	String errorMsg = Utils.getResourceText(context, "msgs", "passwordConfirmation");
            	
            throw new ValidatorException(new FacesMessage(errorMsg));
        }
    }

}