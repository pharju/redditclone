package control;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
 
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import utils.Utils;
 
@ManagedBean(name="languageBean")
@SessionScoped
public class LanguageBean implements Serializable{
	 
	private static final long serialVersionUID = 1L;
	
	private String localeString;
	private Locale locale = Locale.ENGLISH;
	private Locale localees = new Locale("SPANISH");
 
	public LanguageBean(){
		locale = FacesContext.getCurrentInstance()
    			.getViewRoot().getLocale();

		//loop country map to compare the locale code
        for (Map.Entry<String, Object> entry : countries.entrySet()) {

    		// Utils.log("locale : " + locale.toString() + "  map entry: " + entry.getValue().toString());

        	if(entry.getValue().equals(locale)){
        		localeString = entry.getKey();
        		Utils.log("match, localeCode -> " + localeString);
        	}
        }
	}
	
	private static Map<String,Object> countries;
	static{
		countries = new LinkedHashMap<String,Object>();
		countries.put("English", Locale.ENGLISH); //label, value

		countries.put("Spanish", Locale.FRENCH);

		countries.put("German",  Locale.GERMAN);

	}
 
	public Map<String, Object> getCountriesInMap() {
		return countries;
	}
 
 
	public String getLocaleString() {
		return localeString;
	}
 
 
	public void setLocaleString(String localeString) {
		this.localeString = localeString;
	}
 
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}


	//value change event listener
	public void countryLocaleCodeChanged(ValueChangeEvent e){
 
		String newLocaleValue = e.getNewValue().toString();
		
		Utils.log("language -> " + newLocaleValue);
		
		//loop country map to compare the locale code
        for (Map.Entry<String, Object> entry : countries.entrySet()) {
 
        	if(entry.getValue().toString().equals(newLocaleValue)){
 
        		FacesContext.getCurrentInstance()
        			.getViewRoot().setLocale((Locale)entry.getValue());
        		
        		locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
        		localeString = entry.getKey();
        		Utils.log("localeString -> " + localeString);
        	}
        }
	}
}