package model;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;
import java.util.Vector;
import utils.TimeStampCalculation;

import utils.TimeStampCalculation;

public class Comment implements Serializable {
	
	private static int commentIdCounter = 0;	/* class variable ensuring that all comments get a unique ID */
	private int commentId = 0;
	private int commentVoteCount = 0;
	private String commentContent = null;
	private String commentAuthor = null;
	private Date commentTimestamp = null;
	private Submission commentParentSubmission = null;
	private Comment commentParentComment = null;

	public Comment(String content, String author, Submission parentSubmission)
	{
		this.commentContent = content;
		this.commentAuthor = author;
		this.commentTimestamp = new Date();
		this.commentId = createNextCommentId();
		this.commentParentSubmission = parentSubmission;
	}

	/* Note: The vote count should only be altered if the vote is valid! See method checkVoteValidityAndRegisterCommentVote in User-class */
	public void upVote()
	{
		this.commentVoteCount++;
		this.commentParentSubmission.sortSubmissionComments();
	}
	/* Note: The vote count should only be altered if the vote is valid! See method checkVoteValidityAndRegisterCommentVote in User-class */
	public void downVote()
	{
		this.commentVoteCount--;
		this.commentParentSubmission.sortSubmissionComments();
	}
	public int getCommentVoteCount()
	{
		return this.commentVoteCount;
	}
	public int getCommentId()
	{
		return this.commentId;
	}
	public String getCommentContent()
	{
		return this.commentContent;
	}
	public String getCommentAuthor()
	{
		return this.commentAuthor;
	}
	public String getCommentTimestamp()
	{
//		return this.commentTimestamp.toString();
		TimeStampCalculation theTimeStamp = new TimeStampCalculation(this.commentTimestamp);

		return theTimeStamp.CalculateDifferenceToNow();
	}
	private int createNextCommentId() 
	{
		commentIdCounter++;
		return commentIdCounter;
	}
}
