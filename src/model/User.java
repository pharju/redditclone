package model;

import java.io.Serializable;
import java.util.ArrayList;

import utils.VotingHistory;
import utils.VoteType;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

public class User implements Serializable {
	protected String login;

	protected String password;

	private ArrayList<VotingHistory> votedSubmissions; 
	private ArrayList<VotingHistory> votedComments;

	public User()
	{
		this.votedSubmissions = new ArrayList<VotingHistory>();
		this.votedComments = new ArrayList<VotingHistory>();
	}
	public String getLogin() {
		if (login == null) {
			return "";
		}
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		if (password == null) {
			return "";
		}
		return password;
	}
	/* Keeps track of the submissions the user has already voted */
	public boolean checkVoteValidityAndRegisterSubmissionVote(int submissionId, VoteType voteType)
	{
		boolean alreadyVoted = false;
		boolean voteValid = false;
		
		for (VotingHistory vote : this.votedSubmissions)
		{
			if(vote.getId() == submissionId) /* The user has already voted on this */
			{
				alreadyVoted = true;
				
				if(vote.getVoteType() != voteType) /* If the user clicks upvote and then downvote (or vice versa) the vote is negated */
				{
					vote.negateVote();
					voteValid = true;
				}
				else
				{
					/* The user is trying to give several up/downvotes to the same submission! This is not allowed. In this case the vote count must not be changed! */
					voteValid = false;
				}
			}
		}
		if(alreadyVoted == false) /* The user is casting his first vote on this submission */
		{
			this.votedSubmissions.add(new VotingHistory(submissionId, voteType));
			voteValid = true;
		}
		return voteValid;
	}
	/* Keeps track of the comments the user has already voted */
	public boolean checkVoteValidityAndRegisterCommentVote(int commentId, VoteType voteType)
	{
		boolean alreadyVoted = false;
		boolean voteValid = false;
		
		for (VotingHistory vote : this.votedComments)
		{
			if(vote.getId() == commentId) /* The user has already voted on this */
			{
				alreadyVoted = true;
				
				if(vote.getVoteType() != voteType) /* If the user clicks upvote and then downvote (or vice versa) the vote is negated */
				{
					vote.negateVote();
					voteValid = true;
				}
				else
				{
					/* The user is trying to give several up/downvotes to the same comment! This is not allowed. In this case the vote count must not be changed! */
					voteValid = false;
				}
			}
		}
		if(alreadyVoted == false) /* The user is casting his first vote on this comment */
		{
			this.votedComments.add(new VotingHistory(commentId, voteType));
			voteValid = true;
		}
		return voteValid;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean checkPassword(String password) {
		if (this.password.equals(password)) {
			return true;
		}
		return false;
	}
	
	public boolean checkEntries() throws UserException {
		if (login == null && password == null)
			return false;
		if (login == null || login.equals(""))
			throw new UserException();
		if (password == null || password.equals(""))
			throw new UserException();
		return true;
	}
}
