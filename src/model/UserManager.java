package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Hashtable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import utils.Utils;

@ManagedBean(name="userManager")
@ApplicationScoped
public class UserManager implements Serializable {
	
	// Hashtable is synchronized
	protected Hashtable<String, User> users = new Hashtable<String, User>();

	protected String userFilename = "rc_users.ser";
	protected String userFileNameAndPath;

	protected File userFile;

	public UserManager() {

		if (Utils.isMac()) {
			userFileNameAndPath = System.getProperty("user.home") + "/" + userFilename;
		}
		else {
			userFileNameAndPath = System.getProperty("java.io.tmpdir") + userFilename;
		}

		userFile = new File(userFileNameAndPath);

		if (userFile.exists()) {
			Utils.log("opening userfile: " + userFileNameAndPath);
			try {
				FileInputStream fileIn = new FileInputStream(userFile);
				ObjectInputStream objectIn = new ObjectInputStream(fileIn);
				users = (Hashtable<String, User>) objectIn.readObject();
				objectIn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else{
			Utils.log("userfile: " + userFileNameAndPath + " does not exist");
		}
	}

	public boolean register(User user) throws UserException {
		
		synchronized (users) {
			if (users.get(user.getLogin()) != null) { // User existiert bereits
				throw new UserException();
			}
			
			if (user.checkEntries()) { // wirft UserException bei Fehler
				users.put(user.getLogin(), user);
				save();
				return true;
			}
		}
		return false;
	}

	public boolean checkIfFree(String login) {
		if (users.get(login) != null) { // User existiert bereits
			return false;
		}
		return true;
	}

	public User login(String login, String password) throws UserException {
		if ((login == null && password == null)
				|| (login == "" && password == "")) {
			return null;
		}
		User user = users.get(login);
		if (user != null && user.checkPassword(password)) {
			return user;
		}
		throw new UserException();
	}

	public void save() {
		try {
			FileOutputStream fileOut = new FileOutputStream(userFile);
			ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
			objectOut.writeObject(users);
			objectOut.flush();
			objectOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
