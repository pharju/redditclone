package model;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;
import java.util.Vector;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import utils.TimeStampCalculation;
import utils.VoteType;
import control.LoginBean;

public class Submission implements Serializable {
	
	static int submissionIdCounter = 0;
	private int amountOfComments = 0;
	private int submissionId = 0;
	private int submissionVoteCount = 0;
	private String submissionTitle = null;
	private String submissionAuthor = null;
	private Date submissionTimestamp = null;
	private URL submissionLink = null;
	private String submissionLinkString = null;
	private Vector<Comment> submissionComments;

	@ManagedProperty(value="#{loginBean}")
	private LoginBean myLogin;
	
	public Submission(String link, String author, String title)
	{
		try{
			this.submissionLink = new URL(link);
			this.submissionLinkString = this.submissionLink.toString();
			this.submissionAuthor = author;
			this.submissionTitle = title;
			this.submissionTimestamp = new Date();
			this.submissionComments = new Vector<Comment>();
			submissionId = createNextSubmissionId();
		}catch(Exception e)
		{
			//TODO: MalformedURLException should be handled better
			e.printStackTrace();
		}
	}
	private int createNextSubmissionId() 
	{
		submissionIdCounter++;
		return submissionIdCounter;
	}

	public void upVoteSubmission()
	{
		this.submissionVoteCount++;
	}

	public void downVoteSubmission()
	{
		this.submissionVoteCount--;
	}
	
	public int getSubmissionVoteCount()
	{
		return submissionVoteCount;
	}
	public String getSubmissionAuthor()
	{
		return this.submissionAuthor;
	}
	public int getSubmissionId()
	{
		return this.submissionId;
	}
	public String getSubmissionLinkString()
	{
		return this.submissionLinkString;
	}
	public URL getSubmissionLinkURL()
	{
		return this.submissionLink;
	}

	public String getSubmissionTimestamp()
	{
		TimeStampCalculation theTimeStamp = new TimeStampCalculation(this.submissionTimestamp);

		return theTimeStamp.CalculateDifferenceToNow();
	}
	public String getSubmissionTitle()
	{
		return this.submissionTitle;
	}
	public int getAmountOfComments()
	{
		return this.amountOfComments;
	}
	public void addComment(Comment newComment)
	{
		this.amountOfComments++;
		this.submissionComments.add(newComment);
		this.sortSubmissionComments();
	}
	public Vector<Comment> getSubmissionComments() {
		return submissionComments;
	}
	
	public void sortSubmissionComments()
	{
		Comment swap;
		for(int i=0;(i<submissionComments.size()-1);i++)
		{
			for(int j=i+1;j<submissionComments.size();j++)
			{
				if (submissionComments.elementAt(i).getCommentVoteCount() < submissionComments.elementAt(j).getCommentVoteCount())
				{
					swap = submissionComments.elementAt(i);
					submissionComments.setElementAt(submissionComments.elementAt(j), i);
					submissionComments.setElementAt(swap, j);
				}
			}
		}
	}
	public Comment getCommentWithId(int selectedCommentId) {
		for(int i=0;i<submissionComments.size();i++)
		{
			if(submissionComments.get(i).getCommentId() == selectedCommentId)
			{
				return submissionComments.get(i);
			}
		}
		return null;
	}
}