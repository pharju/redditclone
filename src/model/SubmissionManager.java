package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import utils.Utils;


@ManagedBean(name="submissionManager")
@ApplicationScoped
public class SubmissionManager implements Serializable {
	
	private int numbersOfChats;
	Vector<Submission> submissionsVector;
	
	// Hashtable is synchronized
	protected Hashtable<String, Submission> submissions = new Hashtable<String, Submission>();	

	protected String userFilename = "tz_submissions.ser";	
	protected String userFileNameAndPath;

	protected File userFile;

	public void addSubmission(Submission submission){
		
		synchronized (submissions) {
//			Utils.log("adding new chat: " + chat.getName());
			submissions.put(submission.getSubmissionTitle(), submission);
//			save();
		}
	}
	
	public int getNumbersOfSubmissions() {
		return submissions.size();
	}

	public Hashtable<String, Submission> getSubmissions() {
		return submissions;
	}
	public Vector<Submission> getSubmissionsVector()
	{
		submissionsVector = new Vector<Submission>(submissions.values());
		sortSubmissionVector();
		return submissionsVector;
	}
	private void sortSubmissionVector()
	{
		Submission swap;
		for(int i=0;(i<submissionsVector.size()-1);i++)
		{
			for(int j=i+1;j<submissionsVector.size();j++)
			{
				if (submissionsVector.elementAt(i).getSubmissionVoteCount() < submissionsVector.elementAt(j).getSubmissionVoteCount())
				{
					swap = submissionsVector.elementAt(i);
					submissionsVector.setElementAt(submissionsVector.elementAt(j), i);
					submissionsVector.setElementAt(swap, j);
				}
			}
		}
	}
	
	
	public boolean checkIfFree(String submissionTitle) {
		if (submissions.get(submissionTitle) != null) { // Submission Title existiert bereits
			return false;
		}
		return true;
	}
}
